import React from "react";
import Nav from "./components/nav/Nav";
import hotItem from "../src/img/zelda_banner.jpg"
import productThumb from "../src/img/thumb_goku.jpg" //pensar em forma de trazer os itens dinamicamente
import "./global.css";
import "./App.css";
function App() {
  return (
    <div id="app">
      <Nav />
      <div className="principal-banner">
        <h3>Mortal Kombat</h3>
        <div className="generic_price_tag">
          <span className="price">
            <span className="sign">R$</span>
            <span className="currency">299</span>
            <span className="cent">,99</span>
          </span>
        </div>
        <p className="description">
          Mussum Ipsum, cacilds vidis litro abertis. Atirei o pau no gatis, per
          gatis num morreus. Copo furadis é disculpa de bebadis, arcu quam
          euismod magna. Quem num gosta di mim que vai caçá sua turmis! Viva
          Forevis aptent taciti sociosqu ad litora torquent.
        </p>
      </div>
      <div className="hot">
        <img src={hotItem} alt="" />
      </div>
      <div className="product-tray">
        <h4 className="">Produtos em destaque</h4>
        <div className="product">
          <img src={productThumb} alt="" className="product-thumb" />
          <span className="product-name">Action Figure Goku</span>
          <div className="generic_price_tag">
          <span className="price">
            <span className="sign">R$</span>
            <span className="currency">299</span>
            <span className="cent">,99</span>
          </span>
        </div>
        </div>
      </div>
    </div>

  );
}

export default App;
