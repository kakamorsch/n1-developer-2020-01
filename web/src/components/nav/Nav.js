import React from 'react'

import hamburguer from '../../svgs/icon_hamburguer.svg'
import paperPlane from '../../svgs/paper-plane.svg'
import searchSolid from '../../svgs/search-solid.svg'
import shoppingBag from '../../svgs/shopping-bag-solid.svg'
import sliderImage from '../../img/principal_banner_desktop.jpg'
import logo from '../../img/logo_header.png'

import './Nav.css'

function Nav() {

    return (
        <div className="container">
            <nav>
                <div className="hamburguer">
                    <li>
                        <a href=""><img src={hamburguer} alt="hamburguer" color="#FFF"/></a>
                    </li>
                </div>
                <div className='logo'>
                    <li>
                        <img src={logo} alt="logo" />
                    </li>
                </div>
                <div className="nav-wrapper">
                    <ul>
                        <li>
                            <a href=""><img src={paperPlane} alt="Aviao de Papel"/>Contato</a>
                        </li>
                        <li id="center">
                            <a href=""><img src={searchSolid} alt="Lupa"/>Buscar</a>
                        </li>
                        <li>
                            <a href=""><img src={shoppingBag} alt="Sacola"/>Sacola</a>
                        </li>

                    </ul>
                </div>
                        <svg xmlns="http://www.w3.org/2000/svg"></svg>
            </nav>
            <div className="slider-image">
                <img src={sliderImage} alt="Mortal Kombat Scorpion"/>
            </div>
        </div>
    )

}

export default Nav
